# BlockSpace

Welcome to BlockSpace, a simple web application built
 with Django to showcase fundamental web development
 concepts using Python. This application allows users
 to register, log in, and create blog posts.

## Features

- **User Registration:** Sign up for an account.
- **User Login:** Log in to access your dashboard and create blog posts.
- **Blog Posting:** Write and publish blog posts for others to view.

## Technologies

- Python 3
- Django
- HTML/CSS
- PostgreSQL for the database

## Website

You can also view [website](https://mala-prace-mac0571-babc40ff95a5460f3ba488ae23f679625705282695ff.gitlab.io).

## Getting Started

These instructions will help you set up the project
locally for development and testing purposes.

### Prerequisites

You need Python and PostgreSQL installed on your machine.
 You can download Python from [python.org](https://python.org)
 and PostgreSQL from [postgresql.org](https://www.postgresql.org).

### Installing

Follow these steps to set up a development environment:

- Clone the repository:

   ```
   git clone https://gitlab.com/mac0571/mala-prace.git BlockSpace
   ```

- Navigate to the project directory:

   ```
   cd BlockSpace
   ```

- Create and activate a virtual environment:

   ```
   python -m venv env
   source env/bin/activate  # For Unix/macOS
   env\Scripts ctivate.bat # For Windows
   ```

- Install the required packages:

   ```
   pip install -r requirements.txt
   ```

- Configure the database settings in `settings.py`:

   Update the `DATABASES` section with your PostgreSQL credentials.

- Apply database migrations:

   ```
   python manage.py migrate
   ```

- Create a superuser account:

   ```
   python manage.py createsuperuser
   ```

- Run the application:

   ```
   python manage.py runserver
   ```

Open your web browser and visit [http://127.0.0.1:8000](http://127.0.0.1:8000)
 to see the application in action.

## Contributing

Contributions are what make the open-source
 community such an amazing place to learn, inspire,
 and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
1. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
1. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
1. Push to the Branch (`git push origin feature/AmazingFeature`)
1. Open a Pull Request

## License

This project is licensed under the MIT License.

## Acknowledgments

- A hat tip to anyone whose code was used
- Inspiration
- etc.
