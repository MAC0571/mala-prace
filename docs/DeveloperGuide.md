# BlockSpace Developer Guide

Welcome to the BlockSpace Developer Guide! This document
 will help you set up the project and contribute effectively.

## Project Overview

BlockSpace is a web application that allows users to create
 and share blog posts. It's built using Django, a Python web framework.

## Setting Up the Environment

### Prerequisites

Before you begin, make sure you have
 Python 3 and PostgreSQL installed on your machine.

### Installing Dependencies

- Clone the repository:

   ```
   git clone https://gitlab.com/mac0571/mala-prace.git BlockSpace
   ```

- Navigate to the project directory:

   ```
   cd BlockSpace
   ```

- Create and activate a virtual environment:

   ```
   python -m venv env
   source env/bin/activate  # For Unix/macOS
   env\Scriptsctivate.bat # For Windows
   ```

- Install the required packages:

   ```
   pip install -r requirements.txt
   ```

### Configuring the Database

1. Open `settings.py` and locate the `DATABASES` section.
1. Update the settings with your PostgreSQL credentials.

### Running the Application

- Apply database migrations:

   ```
   python manage.py migrate
   ```

- Create a superuser account:

   ```
   python manage.py createsuperuser
   ```

- Run the application:

   ```
   python manage.py runserver
   ```

## Contributing to BlockSpace

### Coding Standards

Please follow PEP 8, the Python style guide, when contributing to BlockSpace.

### Submitting Changes

1. Fork the repository.
1. Create a new branch for your feature.
1. Commit your changes with descriptive commit messages.
1. Push your branch and submit a pull request.

## Contact

For any questions or concerns, reach out to the
 BlockSpace development team at `dev@blockspace.com`.
