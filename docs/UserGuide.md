# BlockSpace User Guide

Welcome to BlockSpace! This user guide will help you navigate
 the features of our web application
 and make the most out of your experience.

## Getting Started

To begin using BlockSpace, simply open your web
 browser and navigate to [http://127.0.0.1:8000](http://127.0.0.1:8000).

### Creating an Account

1. Click on the "Sign Up" button to create a new account.
1. Fill in your username, email, and password.
1. Click "Submit" to register your account.

### Logging In

1. Click on the "Login" button.
1. Enter your username and password.
1. Click "Submit" to access your dashboard.

## Using BlockSpace

### Creating a Blog Post

1. Once logged in, navigate to your dashboard.
1. Click on the "Create Post" button.
1. Fill in the title and content of your blog post.
1. Click "Publish" to make your post live.

### Viewing Blog Posts

1. Visit the homepage to see a list of all published blog posts.
1. Click on a post's title to read the full content.

## Troubleshooting

### Forgotten Password

If you've forgotten your password, click on the "Forgot Password?"
 link on the login page. Follow the instructions to reset your password.

### Contact Support

For additional support, contact our help desk at `support@blockspace.com`.
